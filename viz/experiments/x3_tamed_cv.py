import torch
import argparse
import itertools
import pandas as pd
import numpy as np
import os
from sde_mc import *

torch.set_default_dtype(torch.float64)

dir_path = os.path.dirname(os.path.realpath(__file__))

device = 'cuda' if torch.cuda.is_available() else 'cpu'

print('imports successful')
print(device)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--dir', default=dir_path + '/results', type=str)
    parser.add_argument('--tol', nargs='+', default=[0.0001], type=float)
    parser.add_argument('--init_trials', nargs='+', default=[10000], type=int)
    parser.add_argument('--bs', nargs='+', default=[100000], type=int)
    parser.add_argument('--fname', default='gbm_cv', type=str)
    parser.add_argument('--cv_bs', nargs='+', default=[2000], type=int)
    parser.add_argument('--train_trials', nargs='+', default=[30000], type=int)
    parser.add_argument('--nepochs', nargs='+', default=[20], type=int)
    parser.add_argument('--train_bs', nargs='+', default=[2000], type=int)
    parser.add_argument('--strikes', nargs='+',
                        default=[0], type=float)
    parser.add_argument('--nlayers', nargs='*',
                        default=[3], type=int)
    parser.add_argument('--layers_size', nargs='+', default=[50], type=int)
    parser.add_argument('--layers', nargs='*',
                        action='append', default=None, type=int)
    parser.add_argument('--init_steps', nargs='+', default=[200], type=int)
    parser.add_argument('--nsteps', nargs='+', default=[1000], type=int)
    parser.add_argument('--paired_stepsizes', nargs='?', default=0, type=int)
    parser.add_argument('--init_lr', nargs='+', default=[0.001])
    parser.add_argument('--lr_scheduler', nargs='+',
                        action='append', default=None)
    parser.add_argument('--sigmas', nargs='+',
                        default=np.linspace(0,35,36), type=float)
    parser.add_argument('--ntrials', nargs='+', default=[10**5], type=int)
    args = parser.parse_args()
    

    if args.paired_stepsizes:
        paired_steps = np.stack((args.init_steps, args.nsteps), axis=1)
    else:
        paired_steps = list(itertools.product(args.init_steps, args.nsteps))

    if args.layers is None:
        layers = [np.full(j, i) for i in args.layers_size for j in args.nlayers]
    else:
        layers = args.layers

    if args.lr_scheduler is None:
        lr_scheduler = [None]
    else:
        lr_scheduler = args.lr_scheduler

    fname = args.fname

    solutions = []
    errors = []
    times = []
    train_times = []
    num_trials = []
    batches = []
    strikes = []
    tols = []
    train_trialss = []
    train_batches = []
    epochss = []
    cv_batches = []
    stepss = []
    layerss = []
    lrs = []
    sigmas=[]
    variations=[]
    
    dillon = list(map(tuple,itertools.product(args.tol, args.bs, args.train_trials, args.train_bs,
                 paired_steps, args.init_trials, args.cv_bs, args.nepochs, lr_scheduler, layers, args.strikes, args.ntrials)))
    nargs = len(dillon)
    nargs_tot = len(dillon)*len(args.sigmas)
    
    for j, sigma in enumerate(args.sigmas):
        #Define experiment
        x3 = X012345(sigma**2/2,sigma**2/2,0,-sigma**2/2,0,0,sigma,0,0,0,0,0,torch.tensor([1.]), dim=1)
        solver = TamedEulerSolver(x3, 3, 1000, device=device)
        short_rate = ConstantShortRate(0.0)

        for i, (tol, batch, train_trials, train_batch, steps, init_trials, cv_batch, epochs, lr, layer, strike, trials) in enumerate(dillon):
            #Define nn, optimizer and test function
            f = Mlp(
                2, layer, 1, batch_norm=False, batch_norm_init=True, device=device)
            adam = torch.optim.Adam(
                list(f.parameters()))
            euro_call = lambda x:x
            #Generate realizations for training followed by training of cv
            solver.num_steps = steps[0]
            train_time_start = time.time()
            train_dataloader, cost = simulate_data(train_trials, solver, euro_call, short_rate, bs=train_batch, return_cost=True)
            earlystop = EarlyStopping(0.001,1.96,cost,0.05)
            earlystop.batch_size=train_dataloader.batch_size
            earlystop = EarlyStopping(0.001,1.96,cost,0.05)
            earlystop.batch_size=train_dataloader.batch_size
            _ = train_diffusion_control_variate(
                f, adam, train_dataloader, solver, short_rate, epochs, True, lr, early_stopping=earlystop)
            train_time_end = time.time()
            train_time = train_time_end-train_time_start
            train_times.append(train_time)

            #Main approx.
            solver.num_steps = steps[1]
            mc_stats, mc_paths = mc_apply_cvs3(
                f, solver, trials, euro_call, short_rate, sim_bs=batch, bs=cv_batch, return_paths=True)

            #Collect data
            solutions.append(
                mc_stats.sample_mean)
            errors.append(
                mc_stats.sample_std * 2)
            num_trials.append(trials)
            variations.append(mc_stats.sample_std**2*trials)
            times.append(
                mc_stats.time_elapsed + train_time)
            batches.append(batch)
            strikes.append(strike)
            tols.append(tol)
            train_trialss.append(train_trials)
            train_batches.append(train_batch)
            epochss.append(epochs)
            cv_batches.append(cv_batch)
            stepss.append(steps)
            layerss.append(layers)
            lrs.append(lr)
            sigmas.append(sigma)

            print('experiment successful:', (i+1)+j*nargs, '/', nargs_tot)

    data = pd.DataFrame(
        {'strike': strikes, 'mean': solutions, 'conf_interval': errors, 'time': times, 'training_time': train_times, 'trials': num_trials,
         'batch_size': batches, 'tol': tols, 'training_trials': train_trialss, 'training_batches': train_batches, 'epochs': epochss,
         'cv_batches': cv_batches, 'steps': stepss, 'layers': layerss, 'lr_scheduler': lrs, 'sigma':sigmas, 'Var':variations})

    data.to_csv(args.dir + '/' + fname + '.csv',na_rep='nan')
    print('save successful')
