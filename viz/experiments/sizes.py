import numpy as np
import torch
import argparse
import sys

#Get Batch size given the memory capacity '--limit'.
parser = argparse.ArgumentParser()
parser.add_argument('--steps', default=1000, type=int)
parser.add_argument('--dim', default=3, type=int)
parser.add_argument('--max_bs', default=1000000, type=int)
parser.add_argument('--limit', default=40, type=float)
args = parser.parse_args()

a=torch.tensor([1,2,3])
dtype=sys.getsizeof(a.element_size())
print(dtype,dtype*a.nelement())

gib=np.power(2,30)

for bs in np.arange(1,args.max_bs,1):
    if((args.steps+1)*args.dim*(bs/gib)*dtype>=args.limit):
        print(bs-10,(args.steps+1)*args.dim*((bs-10)/gib)*dtype)
        break
print('MAX: ',args.max_bs,args.limit)
print('done')