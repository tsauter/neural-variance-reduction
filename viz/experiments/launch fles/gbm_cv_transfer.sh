#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=03:30:00

python gbm_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/results --fname gbm_cv_transfer --transfer 1 --strikes 0.7 0.8 0.9 1.0 1.1 1.2 1.3 --bs 1000000
