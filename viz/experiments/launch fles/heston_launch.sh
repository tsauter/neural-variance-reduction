#!/bin/bash
#SBATCH --gpus=1
#SBATCH --mem-per-cpu=16g
#SBATCH --time=00:15:00

python heston.py --dir $HOME/bachelor/sde_mc/experiments/results/cluster --fname test_3_heston --bs 500000
python heston_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/cluster --fname test_4_heston_cv --lr_scheduler ExponentialLR 1.3 --bs 500000