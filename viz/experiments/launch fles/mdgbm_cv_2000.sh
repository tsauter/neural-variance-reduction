#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=03:30:00

python mdgbm_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/real --fname mdgbm_cv_20002 --bs 2000
