#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=03:30:00

python mdgbm_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/results --fname mdgbm_cv_batches --strikes 1.0 --bs 350000 200000 100000 70000 50000 36000 20000 10000
