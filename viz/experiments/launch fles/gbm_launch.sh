#!/bin/bash
#SBATCH --gpus=1
#SBATCH --mem-per-cpu=16g
#SBATCH --time=00:05:00

python gbm.py --dir $HOME/bachelor/sde_mc/experiments/results/cluster --fname test_1_gbm --bs 500000
python gbm_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/cluster --fname test_2_gbm_cv --lr_scheduler ExponentialLR 1.3 --bs 500000