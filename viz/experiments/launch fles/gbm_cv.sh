#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=03:30:00

python gbm_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/results --fname gbm_cv_strikes3 --bs 1000000
