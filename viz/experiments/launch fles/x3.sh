#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=03:30:00

python x3.py --dir $HOME/bachelor/sde_mc/experiments/results/results --fname x3_sigma2 --bs 100000 --ntrials 100000
