#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=23:30:00

python heston.py --dir $HOME/bachelor/sde_mc/experiments/results/real --fname heston_batches --bs 500000 350000 200000 100000 70000 50000 36000 20000 10000 --strikes 1.0
