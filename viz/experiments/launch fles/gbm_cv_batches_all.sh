#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=03:30:00

python gbm_cv_allbs.py --dir $HOME/bachelor/sde_mc/experiments/results/real --fname gbm_cv_batches_all --strikes 1.0 --bs 1000000 700000 500000 350000 200000 100000 70000 50000 36000 20000 10000
