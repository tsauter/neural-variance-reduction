#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=23:30:00

python heston_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/real --fname heston_cv_200022 --bs 2000
