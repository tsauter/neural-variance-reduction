#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=23:30:00

python heston_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/results --fname heston_cv_batches_less --strikes 1.0 --bs 500000 350000 200000 100000 70000 50000 36000 20000 10000 --tol 0.001
