#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=03:30:00

python gbm_mlmc.py --dir $HOME/bachelor/sde_mc/experiments/results/v100 --fname gbm_mlmc_strike
