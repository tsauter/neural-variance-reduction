#!/bin/bash
#SBATCH --gpus=1
#SBATCH --mem-per-cpu=16g
#SBATCH --time=00:10:00

python mdgbm.py --dir $HOME/bachelor/sde_mc/experiments/results/cluster --fname test_5_mdgbm --bs 500000
python mdgbm_cv.py --dir $HOME/bachelor/sde_mc/experiments/results/cluster --fname test_6_mdgbm_cv --lr_scheduler ExponentialLR 1.3 --bs 500000