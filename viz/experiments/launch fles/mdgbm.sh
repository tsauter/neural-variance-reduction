#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus-per-node=v100:1
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=23:30:00

python mdgbm.py --dir $HOME/bachelor/sde_mc/experiments/results/results --fname mdgbm_strikes --bs 350000
