import torch
import argparse
import itertools
import pandas as pd
import numpy as np
import os
from sde_mc import *

torch.set_default_dtype(torch.float64)

dir_path = os.path.dirname(os.path.realpath(__file__))

device = 'cuda' if torch.cuda.is_available() else 'cpu'

print('imports successful')
print(device)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--dir', default=dir_path + '/results', type=str)
    parser.add_argument('--tol', nargs='+', default=[0.0001], type=float)
    parser.add_argument('--init_trials', nargs='+', default=[10000], type=int)
    parser.add_argument('--bs', nargs='+', default=[10000], type=int)
    parser.add_argument('--fname', default='gbm', type=str)
    parser.add_argument('--strikes', nargs='+',
                        default=[0], type=float)
    parser.add_argument('--nsteps', nargs='+', default=[1000], type=int)
    parser.add_argument('--verbose_name', nargs='?', default=0)
    parser.add_argument('--sigmas', nargs='+',
                        default=np.linspace(0,35,36), type=float)
    parser.add_argument('--ntrials', nargs='+', default=[10**5], type=int)
    args = parser.parse_args()

    fname = args.fname

    solutions = []
    errors = []
    times = []
    num_trials = []
    batches = []
    strikes = []
    tols = []
    train_trials = []
    stepss = []
    sigmas = []

    dillon = list(enumerate(map(tuple, itertools.product(args.tol, args.bs,
                                                            args.nsteps, args.init_trials, args.strikes, args.ntrials))))
    nargs = len(dillon)
    nargs_tot = len(dillon)*len(args.sigmas)

    for j, sigma in enumerate(args.sigmas):
        #Define experiment
        x3 = X012345(sigma**2/2,sigma**2/2,0,-sigma**2/2,0,0,sigma,0,0,0,0,0,torch.tensor([1.]), dim=1)
        solver = EulerSolver(x3, 3, 1000, device=device)
        short_rate = ConstantShortRate(0.0)

        for i, (tol, batch, steps, init_trials, strike, trials) in dillon:
            #Define test function
            euro_call = lambda x:x
            #Main approx
            print(trials, sigma)
            solver.num_steps = steps
            mc_stats = mc_simple(trials, solver, euro_call,
                                discounter=short_rate, bs=batch)
            #Collect data
            solutions.append(mc_stats.sample_mean)
            errors.append(mc_stats.sample_std * 2)
            num_trials.append(trials)
            times.append(mc_stats.time_elapsed)
            batches.append(batch)
            strikes.append(strike)
            tols.append(tol)
            train_trials.append(train_trials)
            stepss.append(steps)
            sigmas.append(sigma)

            print('experiment successful:', (i+1)+j*nargs, '/', nargs_tot)

    data = pd.DataFrame(
        {'strike': strikes, 'mean': solutions, 'conf_interval': errors, 'time': times, 'trials': num_trials, 'batch_size': batches, 'steps': stepss, 'sigma':sigmas})
    
    data.to_csv(args.dir + '/' + fname + '.csv',na_rep='NaN')
    print('save successful')
