import torch
import argparse
import itertools
import pandas as pd
import numpy as np
import os
from sde_mc import *

dir_path = os.path.dirname(os.path.realpath(__file__))

device = 'cuda' if torch.cuda.is_available() else 'cpu'

print('imports successful')
print(device)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--dir', default=dir_path + '/results', type=str)
    parser.add_argument('--tol', nargs='+', default=[0.0001], type=float)
    parser.add_argument('--init_trials', nargs='+', default=[10000], type=int)
    parser.add_argument('--bs', nargs='+', default=[10000], type=int)
    parser.add_argument('--fname', default='heston', type=str)
    parser.add_argument('--strikes', nargs='+',
                        default=np.linspace(0.0, 4.0, 41, dtype=np.single), type=float)
    parser.add_argument('--nsteps', nargs='+', default=[1000], type=int)
    args = parser.parse_args()

    #Define experiment
    heston = TruncatedHeston(0.02, 0.25, 0.5, 0.3, -0.3, torch.tensor([1, 0.15]))
    solver = EulerSolver(heston, 3, 1000, device=device)
    short_rate = ConstantShortRate(heston.r)
    levels = [16, 64, 256, 1024]

    fname = args.fname

    solutions = []
    errors = []
    times = []
    num_trials = []
    batches = []
    strikes = []
    tols = []
    train_trials = []
    stepss = []

    dillon = list(enumerate(map(tuple, itertools.product(args.tol, args.bs,
                                                         args.nsteps, args.init_trials, args.strikes))))
    nargs = len(dillon)

    for i, (tol, batch, steps, init_trials, strike) in dillon:
        print(strike)
        #Define test function
        euro_call = EuroCall(strike)
        #Estimate sample size for each level
        solver.num_steps = steps
        trials = get_optimal_trials(init_trials,levels,tol,solver,euro_call,short_rate)
        print(trials)
        #Get batch sizes
        mlmc_batch = mlmc_bs_from_trials(trials, levels, max_mem=3*10**8)
        #Main approx.
        mc_stats = mc_multilevel(trials, levels, solver, euro_call,
                             discounter=short_rate, bs=mlmc_batch)
        #Collect data
        solutions.append(mc_stats.sample_mean)
        errors.append(mc_stats.sample_std * 2)
        num_trials.append(trials)
        times.append(mc_stats.time_elapsed)
        batches.append(batch)
        strikes.append(strike)
        tols.append(tol)
        train_trials.append(train_trials)
        stepss.append(steps)

        print('experiment successful:', (i+1), '/', nargs)

    data = pd.DataFrame(
        {'strike': strikes, 'mean': solutions, 'conf_interval': errors, 'time': times, 'trials': num_trials, 'batch_size': batches, 'steps': stepss})

    data.to_csv(args.dir + '/' + fname + '.csv')
    print('save successful')
